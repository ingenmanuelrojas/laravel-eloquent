<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function posts(){
        //Un categoria tiene muchos post
        return $this->hasMany(Post::class);
    }

    public function videos(){
        //Un categoria tiene muchos videos
        return $this->hasMany(Video::class);
    }
}
