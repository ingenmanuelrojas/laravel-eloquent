<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    public function users(){
        //Un nivel tiene muchos usuarios
        return $this->hasMany(Users::class);
    }

    public function posts(){
        //Un nivel tiene muchos posts a travez de usuarios
        return $this->hasManyThrough(Post::class, User::class);
    }

    public function videos(){
        //Un nivel tiene muchos videos a travez de usuarios
        return $this->hasManyThrough(Video::class, User::class);
    }
}
