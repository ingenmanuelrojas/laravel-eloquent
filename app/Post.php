<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user(){
        //Un post Pertenece a un usuario
        return $this->belongsTo(User::class);
    }
    
    public function category(){
        //Un post Pertenece a una categoria
        return $this->belongsTo(Category::class);
    }

    public function comments(){
        //La palabra commentable es la que se uso en la creacion de la migracion de la tabla comment
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }

    public function tags(){
        return $this->morphToMany(Tag::class, 'taggable');
    }

}
