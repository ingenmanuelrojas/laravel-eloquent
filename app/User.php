<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        //Un usuario tiene un solo perfil
        return $this->hasOne(Profile::class);
    }

    public function level(){
        //Un usuario pertenece a un nivel
        return $this->belongsTo(Level::class);
    }

    public function groups(){
        //Un usuario pertenece y tiene muchos grupos
        return $this->belongsToMany(Group::class)->withTimestamps();
    }

    public function location(){
        //Un usuario tiene una localizacion a travez de perfil
        return $this->hasOneThrough(Location::class, Profile::class);
    }

    public function posts(){
        //Un usuario tiene muchos posts
        return $this->hasMany(Post::class);
    }
    
    public function videos(){
        //Un usuario tiene muchos videos
        return $this->hasMany(Video::class);
    }

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }
}
